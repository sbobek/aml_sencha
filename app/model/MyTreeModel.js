Ext.define('HelloWorld.model.MyTreeModel', {
    extend: 'Ext.data.Model',
    config:{
    fields: ['author', 'title', 'description', 'url', {
                name: 'leaf',
                defaultValue: true
            }]
    }
});
