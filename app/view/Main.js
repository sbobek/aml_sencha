Ext.define('HelloWorld.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar',
         'HelloWorld.store.NewsStore',
        'Ext.dataview.NestedList',
        'HelloWorld.controller.MainController'
    ],
    config: {
        tabBarPosition: 'bottom',
       
        items: [
            {
                title: 'Welcome',
                iconCls: 'home',
                styleHtmlContent: true,
                scrollable: true,
               

                html: [
                    "<center>Hello World!<br> Tap button at the bottom to show the image of a day!</center>"
                ].join(""),

                items: [{
                        docked: 'top',
                        xtype: 'titlebar',
                        title: 'Hello World'
                    },
                    {
                        xtype: 'image',
                        id: 'imageref',
                        src: 'http://d39kbiy71leyho.cloudfront.net/wp-content/uploads/2016/05/09170020/cats-politics-TN.jpg',
                        mode: 'image'
                    },
                    {
                        xtype: 'button',
                        action: 'showImage',
                        text: 'Show Image Of A Day',
                        docked: 'bottom',
                        align: 'center'
                    }
                ]

    
            },
            {
                title: 'News',
                iconCls: 'info',
                xtype: 'nestedlist',
                store: 'NewsStore',
                title: 'News',
                displayField: 'title',

       

                detailCard: {
                    xtype: 'panel',
                    scrollable: true,
                    styleHtmlContent: true
                },

                 listeners: {
                        itemtap: function(nestedList, list, index, element, post) {
                            this.getDetailCard().setHtml(
                                [post.get('description'),"<br>See more: <a href=",post.get('url'),">TechCrunch</a>"].join(""));
                        }
                    }
                
            }
        ]
    }
});
