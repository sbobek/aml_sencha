Ext.define('HelloWorld.store.NewsStore', {
extend: 'Ext.data.TreeStore', 
requires: ['Ext.data.proxy.Ajax'],
id: 'NewsStore',
config:{
            model: 'HelloWorld.model.MyTreeModel',
            autoLoad: true,
//            fields: ['author', 'title', 'description', 'url', {
//                name: 'leaf',
//                defaultValue: true
//            }],

            root: {
                leaf: false
            },


            //data: [
            //    {author: 'Szymon Bobek',title:'Wielkie dzieło',description:'Blabla bla blabla blabla',url:'geist.re'},
            //    {author: 'Krzysztof Kluza',title:'Wiekopomne dzieło',description:'Blabla blabl blabalbal blabla',url:'geist.re'}
            //]
 
            proxy: {
                type: 'ajax',
                url: 'https://newsapi.org/v1/articles?source=techcrunch&apiKey=abe648b1a17a46df9609bd7778ba7522',
                reader: {
                    type: 'json',
                    rootProperty: 'articles'
                }
            }
        }
});
