Ext.define('HelloWorld.controller.MainController', {
    extend: 'Ext.app.Controller',

    config: {
        control: {
            catButton: {
                tap: 'showcat'
            }
        },

        refs: {
            catButton: 'button[action=showImage]'
        }
    },

    showcat: function() {
        // called whenever the Login button is tapped
        console.log('Button tapped'),
        Ext.getCmp('imageref').setSrc('https://upload.wikimedia.org/wikipedia/commons/4/47/American_Eskimo_Dog.jpg');

    }
});
